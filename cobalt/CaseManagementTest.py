from logging import error
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
from Helpers.APIHelpers import APIHelpers
import Helpers.Login as Login
from Helpers.TestManager import TestManager

from random import choice
from string import ascii_uppercase

import string    
import random

#Test Case ID: 175263

class CaseManagementTest(unittest.TestCase):

		def setUp(self):
			fireFoxOptions = webdriver.FirefoxOptions()
			fireFoxOptions.headless = True
			self.driver  = webdriver.Firefox(options=fireFoxOptions)

			base_url = TestManager().get_base_url()
			self.url = base_url + "/#/dashboard/home"
			self.log_in = Login.AppLogin(self.driver)

		def test_perm_case(self):
			login_browser = self.log_in.admin_login()
			assert not login_browser is False 
			browser = self.driver
			browser.get(self.url)

			time.sleep(10)

			username = "ATYFSTUDBH"
			APIHelpers().createBeneficiaryWithCase(username)
			time.sleep(3)

			#Choose admin tab
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Admin')))

			element.click()

			time.sleep(2)

			#Choose company
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

			element.click()

			time.sleep(2)

			#CHoose tab
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

			element.click()

			time.sleep(2)

			#Find user
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.NAME, 'fullName')))

			element.send_keys(username)

			time.sleep(2)

			#Click on cog
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

			element.click()

			time.sleep(2)

			#Click on profile
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > .ng-scope:nth-child(2)')))

			element.click()

			time.sleep(2)

			#Click on cases
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Cases')))

			element.click()

			time.sleep(2)

			#Click on Case
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, 'div.list-body:nth-child(1)')))

			element.click()

			time.sleep(4)

			#Click on overview
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Overview')))

			element.click()

			time.sleep(2)

			time.sleep(2)

			#Click category dropdown
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, "div.form-section:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")))

			element.click()

			time.sleep(2)

			#Click category
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[2]/div/div/div/div/div/div[2]/div/loading-aware-panel/div[2]/div/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div/input[1]")))

			element.send_keys("Consultation")
			element.send_keys(Keys.RETURN)

			time.sleep(2)

			#Click case type dropdown
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, "div.form-section:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")))

			element.click()

			time.sleep(2)

			#Click case type
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[2]/div/div/div/div/div/div[2]/div/loading-aware-panel/div[2]/div/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div/input[1]")))

			element.send_keys("Consultation")
			element.send_keys(Keys.RETURN)

			time.sleep(2)

			#Click on case management
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Case Management')))

			element.click()

			time.sleep(2)

			#browser.find_element_by_xpath("//h1[contains(.,'PERM')]")
			browser.find_element_by_xpath("//span[contains(.,'Task')]")
			browser.find_element_by_xpath("//i[contains(.,'Start')]")
			browser.find_element_by_xpath("//i[contains(.,'Due')]")
			browser.find_element_by_xpath("//i[contains(.,'Completed')]")
			browser.find_element_by_xpath("//span[contains(.,'Report Notes (Visible to Clients)')]")


			#Unclick tasks
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.ID, 'check-tasks')))

			element.click()

			time.sleep(2)
			error_raised = False

			#browser.find_element_by_css_selector(".ng-scope:nth-child(2) > .header-section > .header-list-task-main")

			try:
					#Unclick tasks
					element = WebDriverWait(browser, 5).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-scope:nth-child(2) > .header-section > .header-list-task-main")))

			except TimeoutException:
					error_raised = True

			assert error_raised == True

			#Unclick reports
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.ID, 'check-report-notes')))

			element.click()

			time.sleep(2)

			error_raised = False

			try:
					#Unclick tasks
					element = WebDriverWait(browser, 5).until(
					EC.element_to_be_clickable((By.XPATH, "//span[contains(.,'Report Notes (Visible to Clients)')]")))
			except NoSuchElementException:
					error_raised = True
			except TimeoutException:
					error_raised = True

			assert error_raised == True

			#Click tasks again
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.ID, 'check-tasks')))

			element.click()

			time.sleep(2)

			#browser.find_element_by_css_selector(".ng-scope:nth-child(2) > .header-section > .header-list-task-main")

			#Click reports
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.ID, 'check-report-notes')))

			element.click()

			time.sleep(2)

			browser.find_element_by_xpath("//span[contains(.,'Report Notes (Visible to Clients)')]")

		def test_no_type_selected(self):
			login_browser = self.log_in.admin_login()
			assert not login_browser is False 
			browser = self.driver
			browser.get(self.url)

			time.sleep(8)
			
			username = "ATYFSTUDBH"
			APIHelpers().createBeneficiaryWithCase(username)
			time.sleep(3)
			
			#CHoost admin tab
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Admin')))

			element.click()

			time.sleep(2)

			#Choose company
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

			element.click()

			time.sleep(2)

			#CHoose tab
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

			element.click()

			time.sleep(2)

			#Find user
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.NAME, 'fullName')))

			element.send_keys(username)

			time.sleep(2)

			#Click on cog
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

			element.click()

			time.sleep(2)

			#Click on profile
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > .ng-scope:nth-child(2)')))

			element.click()

			time.sleep(2)

			#Click on cases
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Cases')))

			element.click()

			time.sleep(2)

			#Click Case
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-body:nth-child(1) > .text-center:nth-child(3)')))

			element.click()

			time.sleep(4)

			# element.click()

			time.sleep(2)

			#Ensure close button is present
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.close-info > .btn')))

			element.click()

			time.sleep(2)

			#Ensure close button is present
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-times')))

			element.click()

			time.sleep(2)

		def test_task_functionality(self):
			login_browser = self.log_in.admin_login()
			assert not login_browser is False
			browser = self.driver
			browser.get(self.url)

			task_name = ''.join(choice(ascii_uppercase) for i in range(6))
			task_date = "09082022"

			time.sleep(2)

			username = "ATYFSTUDBH"
			APIHelpers().createBeneficiaryWithCase(username)
			time.sleep(3)

			#Choose admin tab
			element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Admin')))

			element.click()

			time.sleep(2)

			#Choose company
			element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

			element.click()

			time.sleep(2)

			#CHoose tab
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

			element.click()

			time.sleep(2)

			#Find user
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.NAME, 'fullName')))

			element.send_keys(username)

			time.sleep(2)

			#Click on cog
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

			element.click()

			time.sleep(2)

			#Click on profile
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > .ng-scope:nth-child(2)')))

			element.click()

			time.sleep(2)

			#Click on cases
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Cases')))

			element.click()

			time.sleep(2)

			#Click Case
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.flex-item:nth-child(3) > .col-md-2:nth-child(2)')))

			element.click()

			time.sleep(4)

			#Click on overview
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Overview')))

			element.click()

			time.sleep(2)

			#Click category dropdown
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, "div.form-section:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")))

			element.click()

			time.sleep(2)

			#Click category
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[2]/div/div/div/div/div/div[2]/div/loading-aware-panel/div[2]/div/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div/input[1]")))

			element.send_keys("Consultation")
			element.send_keys(Keys.RETURN)

			time.sleep(2)

			#Click case type dropdown
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, "div.form-section:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")))

			element.click()

			time.sleep(2)

			#Click category
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[2]/div/div/div/div/div/div[2]/div/loading-aware-panel/div[2]/div/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div/input[1]")))

			element.send_keys("Consultation")
			element.send_keys(Keys.RETURN)

			time.sleep(2)

			#Click on case management
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Case Management')))

			element.click()

			time.sleep(2)

			#browser.find_element_by_xpath("//span[contains(.,'Tasks')]")

			#Unclick tasks
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.ID, 'check-tasks')))
			time.sleep(1)
			element.click()

			time.sleep(2)
			error_raised = False

			try:
					#Check tasks unclicked
					element = WebDriverWait(browser, 5).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-scope:nth-child(2) > .header-section > .header-list-task-main")))

			except TimeoutException:
					error_raised = True

			assert error_raised == True

			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.ID, 'check-tasks')))

			element.click()

			time.sleep(2)

			#element = WebDriverWait(browser, 20).until(
			#        EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-isolate-scope .ui-select-placeholder')))

			#element.click()

			time.sleep(2)

			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-isolate-scope .ui-select-placeholder')))
			
			element.click()

			time.sleep(2)

			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[2]/div/div/div/div/div/div[2]/div/div[1]/div[2]/div[3]/new-task-form/div/div/form/div[1]/div/input[1]')))

			element.send_keys(task_name)

			time.sleep(2)

			element = WebDriverWait(browser, 20).until( 
					EC.element_to_be_clickable((By.NAME, 'taskDueDate')))

			element.send_keys(task_date)

			time.sleep(2)

			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-valid-required > .due-date-col > .btn')))

			element.click()

			time.sleep(2)

			#Hover over cog
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[id*=btn-urgent-switch]')))

			hover = ActionChains(browser).move_to_element(element)
			hover.perform()

			time.sleep(2)

			#Make task urgent
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Mark Urgent')))

			element.click()

			time.sleep(2)

			#Move to hover on another arbitrary element

			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn-center')))

			hover = ActionChains(browser).move_to_element(element)
			hover.perform()

			time.sleep(2)

			#Hover on cog again

			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[id*=btn-urgent-switch]')))

			hover = ActionChains(browser).move_to_element(element)
			hover.perform()

			time.sleep(2)

			#Make not urgent
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Not Urgent')))

			element.click()

			time.sleep(2)

			#CLose task
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, '//span/i[2]')))

			element.click()

			time.sleep(2)

			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.ID, 'cbxReportNoteOption_NoNewReportNote')))

			element.click()

			time.sleep(2)

			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.modal-footer > button:nth-child(1)')))

			element.click()

			time.sleep(2)

		def test_report_notes(self):
			login_browser = self.log_in.admin_login()
			assert not login_browser is False
			browser = self.driver
			browser.get(self.url)

			report_note = ''.join(choice(ascii_uppercase) for i in range(6))
			task_date = "09082022"

			time.sleep(10)

			username = "ATYFSTUDBH"
			APIHelpers().createBeneficiaryWithCase(username)
			time.sleep(3)

			#CHoost admin tab
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-scope:nth-child(3) > a')))

			element.click()

			time.sleep(2)

			#Choose company
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

			element.click()

			time.sleep(2)

			#CHoose tab
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

			element.click()

			time.sleep(2)

			#Find user
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.NAME, 'fullName')))

			element.send_keys(username)

			time.sleep(2)

			#Click on cog
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

			element.click()

			time.sleep(2)

			#Click on profile
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > .ng-scope:nth-child(2)')))

			element.click()

			time.sleep(2)

			#Click on cases
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Cases')))

			element.click()

			time.sleep(2)

			#Click Case
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.flex-item:nth-child(2) > .col-md-2:nth-child(2) > .ng-binding')))

			element.click()

			time.sleep(2)

			#Click to add note
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.case-task-new > .task-list .ui-select-placeholder')))

			element.click()

			time.sleep(2)

			#Write note
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, "(//input[@type='search'])[2]")))

			element.send_keys(report_note)

			time.sleep(2)

			#Submit
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.new-report-note > .due-date-col > .btn')))

			element.click()

			time.sleep(2)

			browser.find_element_by_xpath("//span[contains(.,'" + report_note +"')]")

			#Unclick reports
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.ID, 'check-report-notes')))

			element.click()

			time.sleep(2)

			error_raised = False

			try:
					#Check reports gone
					element = WebDriverWait(browser, 5).until(
					EC.element_to_be_clickable((By.XPATH, "//span[contains(.,'Report Notes (Visible to Clients)')]")))
			except NoSuchElementException:
					error_raised = True
			except TimeoutException:
					error_raised = True

			assert error_raised == True

			#Click reports
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.ID, 'check-report-notes')))

			element.click()

			time.sleep(2)

			#Check note still present
			browser.find_element_by_xpath("//span[contains(.,'" + report_note +"')]")

		def test_nobill_close_case(self):
			login_browser = self.log_in.admin_login()
			assert not login_browser is False 
			browser = self.driver
			browser.get(self.url)

			time.sleep(10)

			username = "ATYFSTUDBH"
			APIHelpers().createBeneficiaryWithCase(username)
			time.sleep(3)

			#Choose admin tab
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Admin')))

			element.click()

			time.sleep(2)

			#Choose company
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

			element.click()

			time.sleep(2)

			#CHoose tab
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

			element.click()

			time.sleep(2)

			#Find user
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.NAME, 'fullName')))

			element.send_keys(username)

			time.sleep(2)

			#Click on cog
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

			element.click()

			time.sleep(2)

			#Click on profile
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > .ng-scope:nth-child(2)')))

			element.click()

			#time.sleep(2)

			#Click on cases
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Cases')))

			element.click()

			time.sleep(2)

			#Click Case
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, '.flex-item:nth-child(2) > .col-md-2:nth-child(2) > .ng-binding')))

			element.click()

			time.sleep(4)

			#Click on overview
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Overview')))

			element.click()

			time.sleep(2)

			#Click category dropdown
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, "div.form-section:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")))

			element.click()

			time.sleep(2)

			#Click category
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[2]/div/div/div/div/div/div[2]/div/loading-aware-panel/div[2]/div/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div/input[1]")))

			element.send_keys("Consultation")
			element.send_keys(Keys.RETURN)

			time.sleep(2)

			#Click case type dropdown
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, "div.form-section:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")))

			element.click()

			time.sleep(2)

			#Click category
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[2]/div/div/div/div/div/div[2]/div/loading-aware-panel/div[2]/div/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div/input[1]")))

			element.send_keys("Consultation")
			element.send_keys(Keys.RETURN)

			time.sleep(2)

			#Click on case management
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, 'Case Management')))

			element.click()

			time.sleep(2)

			#Click No Bill
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, "//label[contains(.,'No Bill')]")))

			element.click()

			time.sleep(2)

			#Click to close
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[2]/div/div/div/div/div/div[2]/div/div[2]/div/button")))

			element.click()

			time.sleep(2)

			#Close
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer > .btn")))

			element.click()

			time.sleep(2)

			#Close
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, "div:nth-child(2) > .btn")))

			element.click()

			time.sleep(2)

			browser.find_element_by_css_selector(".img-closed-tag")

			#Go to case management
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.LINK_TEXT, "Case Management")))

			element.click()

			time.sleep(2)

			#Click OK
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, "button.ng-binding")))

			element.click()

			time.sleep(2)

			#Click to reopen case
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, ".close-info > div:nth-child(3) > button:nth-child(1)")))

			element.click()

			time.sleep(2)

			#Confirm reopen
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer > .btn:nth-child(1)")))

			element.click()

			time.sleep(2)

			#Click ok
			element = WebDriverWait(browser, 20).until(
					EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer > .btn")))

			element.click()

			time.sleep(2)

			case_closed = False

			try: 
					browser.find_element_by_css_selector(".img-closed-tag")
			except TimeoutException:
					case_closed = True
			except NoSuchElementException:
					case_closed = True

			assert case_closed is True

if __name__ == "__main__":
	unittest.main()

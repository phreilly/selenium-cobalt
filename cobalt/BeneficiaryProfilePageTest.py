import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Helpers.Login as Login
from random import choice
from string import ascii_uppercase
import random
import string
from Helpers.TestManager import TestManager
from Helpers.APIHelpers import APIHelpers


#Test Case ID: 175253
class BeneficiaryProfilePageTest(unittest.TestCase):

	def setUp(self):
		fireFoxOptions = webdriver.FirefoxOptions()
		fireFoxOptions.headless = True
		self.driver  = webdriver.Firefox(options=fireFoxOptions)
		#self.driver  = webdriver.Firefox()
		self.driver.maximize_window()
		base_url = TestManager().get_base_url()
		self.url = base_url + "/#/dashboard/home"
		self.hr_contact_url = base_url + "/#/dashboard/home"
		self.log_in = Login.AppLogin(self.driver)

	def test_edit_dependant(self):
		login_browser = self.log_in.admin_login()
		assert not login_browser is False
		browser = self.driver
		browser.get(self.url)

		fname = ''.join(choice(ascii_uppercase) for i in range(6))

		time.sleep(2)

		#Choose admin tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Admin')))

		element.click()

		time.sleep(2)

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		time.sleep(2)

		#CHoose tab
		element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)
        
		username = "ATYFSTUDBH"
		APIHelpers().createBeneficiaryWithCase(username)
		time.sleep(3)

        #Search user
		element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'fullName')))

		time.sleep(4)

		element.send_keys(username)

		time.sleep(2)

        #Open settings
		element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

		element.click()

		time.sleep(2)

		#Go to profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.open .ng-scope:nth-child(2)')))

		element.click()

		time.sleep(2)

		#Edit
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, 'firstName')))

		element.clear()
		element.send_keys(username)

		time.sleep(2)

		#Click away
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.form-section:nth-child(1)')))

		element.click()

		time.sleep(2)

		#Click new tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Immigration Documents')))

		element.click()

		time.sleep(2)

		#Click back to tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Profile')))

		element.click()

		time.sleep(2)

		username = "ATYFSTUDBH"
		APIHelpers().createBeneficiaryWithCase(username)
		time.sleep(3)

		#browser.find_element_by_xpath("//span[contains(.,'" + fname + " " + username + "')]")

		assert "profile/" in browser.current_url

		time.sleep(1)

	def test_close_profile(self):
		login_browser = self.log_in.admin_login()
		assert not login_browser is False
		browser = self.driver
		browser.get(self.url)

		time.sleep(2)

		#CHoost admin tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-scope:nth-child(3) > a')))

		element.click()

		time.sleep(2)

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		time.sleep(2)

		#CHoose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		username = "ATYFSTUDBH"
		APIHelpers().createBeneficiaryWithCase(username)
		time.sleep(3)

		#Search user
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, 'fullName')))

		element.send_keys(username)

		time.sleep(1)

		#Click away
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.col-md-9')))

		element.click()

		time.sleep(1)

		#Open settings
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) .fa')))

		element.click()

		time.sleep(2)

		#Go to profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.open .ng-scope:nth-child(2)')))

		element.click()

		time.sleep(2)

		#Click settings
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

		element.click()

		time.sleep(2)

		#Click close profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > li')))

		element.click()

		time.sleep(2)

		#Confirm close
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.modal-footer > .btn:nth-child(1)')))

		element.click()

		time.sleep(2)

		browser.find_element_by_css_selector(".img-closed-tag-bottom")

		#Go back to company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.bal-userName-content-direct')))
		time.sleep(1)
		element.click()

		time.sleep(2)

		#Go to client contacts
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		#Find user
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, 'fullName')))

		element.send_keys(username)

		time.sleep(2)

		#Unselect active only
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.bal-caseManagerment-title')))

		element.click()

		time.sleep(2)

		#Click on cog
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) .fa')))

		element.click()

		time.sleep(2)

		browser.find_element_by_xpath("//li[contains(.,'Re-open')]")

		#Reopen user
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.open .ng-scope')))

		element.click()

		time.sleep(2)

		#Confirm reopen
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.modal-footer > .btn')))

		element.click()

		time.sleep(2)

	def test_verify_profile_accessible_hr_contact(self):
			login_browser = self.log_in.hr_contact_login()
			assert not login_browser is False
			browser = self.driver
			browser.get(self.hr_contact_url)

			fname = ''.join(choice(ascii_uppercase) for i in range(6))

			time.sleep(10)

			username = "ATYFSTUDBH"
			APIHelpers().createBeneficiaryWithCase(username)
			time.sleep(3)

			#Click case
			element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, username + " " + username)))

			element.click()

			time.sleep(2)

			#Click Beneficiary
			element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.bal-userName-content-direct')))

			element.click()

			time.sleep(2)

			#Click new tab
			element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Immigration Documents')))

			element.click()

			time.sleep(2)

			#Click back to tab
			element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Profile')))

			element.click()

			time.sleep(2)

			assert "profile/" in browser.current_url

			time.sleep(1)

	def test_verify_profile_accessible_attorey(self):
		login_browser = self.log_in.attorney_login()
		assert not login_browser is False
		browser = self.driver
		browser.get(self.url)

		fname = ''.join(choice(ascii_uppercase) for i in range(6))

		username = "ATYFSTUDBH"
		APIHelpers().createBeneficiaryWithCase(username)
		time.sleep(3)

		#Choose admin tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Admin')))

		element.click()

		time.sleep(2)

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.col-md-4:nth-child(1)')))

		element.click()

		time.sleep(2)

		#Choose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		#Search user
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, 'fullName')))

		element.send_keys(username)

		time.sleep(2)

		#Click away
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.col-md-9')))

		element.click()

		time.sleep(2)

		#Open settings
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

		element.click()

		time.sleep(2)

		#Go to profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.open .ng-scope:nth-child(2)')))

		element.click()

		time.sleep(2)

		#Edit
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, 'firstName')))

		element.click()
		time.sleep(2)
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, 'form.ng-dirty > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > input:nth-child(2)')))

		element.clear()
		element.send_keys(username)
		
		time.sleep(2)

		#Click new tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Immigration Documents')))

		element.click()

		time.sleep(2)

		#Click back to tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Profile')))

		element.click()

		time.sleep(2)

		assert "profile/" in browser.current_url

		time.sleep(1)


	# def test_verify_profile_accessible_beneficiary(self):
	# 		login_browser = self.log_in.beneficiary_login()
	# 		assert not login_browser is False
	# 		browser = self.driver
	# 		browser.get(self.hr_contact_url)

	# 		fname = ''.join(choice(ascii_uppercase) for i in range(6))
	# 		username = "ATYFSTUDBH"
	# 		APIHelpers().createBeneficiaryWithCase(username)
	# 		time.sleep(3)

	# 		time.sleep(10)

	# 		#Click case
	# 		element = WebDriverWait(browser, 20).until(
	# 			EC.element_to_be_clickable((By.LINK_TEXT, 'Consultation')))

	# 		element.click()

	# 		time.sleep(2)

	# 		#Click Beneficiary
	# 		element = WebDriverWait(browser, 20).until(
	# 			EC.element_to_be_clickable((By.CSS_SELECTOR, '.bal-userName-content-direct')))

	# 		element.click()

	# 		time.sleep(2)

	# 		#Go to Family Member
	# 		element = WebDriverWait(browser, 20).until(
	# 			EC.element_to_be_clickable((By.CSS_SELECTOR, '.bal-quickView-title:nth-child(1) > .ng-binding:nth-child(1)')))

	# 		element.click()

	# 		time.sleep(2)

	# 		#Edit
	# 		element = WebDriverWait(browser, 20).until(
	# 			EC.element_to_be_clickable((By.NAME, 'firstName')))

	# 		element.clear()
	# 		element.send_keys(fname)

	# 		time.sleep(2)

	# 		#Click new tab
	# 		element = WebDriverWait(browser, 20).until(
	# 			EC.element_to_be_clickable((By.LINK_TEXT, 'Immigration Documents')))

	# 		element.click()

	# 		time.sleep(2)

	# 		#Click back to tab
	# 		element = WebDriverWait(browser, 20).until(
	# 			EC.element_to_be_clickable((By.LINK_TEXT, 'Profile')))

	# 		element.click()

	# 		time.sleep(2)

	# 		browser.find_element_by_xpath("//span[contains(.,'" + fname + "')]")

	# 		assert "profile/" in browser.current_url

	# 		time.sleep(1)


if __name__ == '__main__':
	unittest.main()
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Helpers.Login as Login
from Helpers.TestManager import TestManager

#Test Case ID: 175262
class TimeLinePageTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        self.driver  = webdriver.Firefox(options=fireFoxOptions)

        base_url = TestManager().get_base_url()
        self.url = base_url + "/#/dashboard/home"
        self.log_in = Login.AppLogin(self.driver)

    def test_page_elements_beneficiary(self):
        login_browser = self.log_in.beneficiary_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        time.sleep(2)

        #Click case
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.col-md-4:nth-child(1) .card-title')))

        element.click()

        time.sleep(2)

        browser.find_element_by_xpath("//span[contains(.,'Start')]")
        browser.find_element_by_xpath("//span[contains(.,'Docs')]")
        browser.find_element_by_xpath("//span[contains(.,'Close')]")
        browser.find_element_by_xpath("//span[contains(.,'Docs')]")

        assert "timeline" in browser.current_url

if __name__ == '__main__':
    unittest.main()
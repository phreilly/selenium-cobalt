import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Helpers.Login as Login
from Helpers.APIHelpers import APIHelpers
from Helpers.TestManager import TestManager

#Test Case ID: 175259

class BeneficiaryDocumentsPageTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        self.driver  = webdriver.Firefox(options=fireFoxOptions)
        #self.driver = webdriver.Firefox()
        
        base_url = TestManager().get_base_url()
        self.url = base_url + "/#/dashboard/home"
        self.log_in = Login.AppLogin(self.driver)

    def test_page_elements(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        time.sleep(2)

        #Choose admin tab
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-scope:nth-child(3) > a')))

        element.click()

        time.sleep(2)

        #Choose company
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

        element.click()

        time.sleep(2)

        #CHoose tab
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

        element.click()

        time.sleep(2)
        
        username = "ATYFSTUDBH"
        APIHelpers().createBeneficiaryWithCase(username)
        time.sleep(3)

        #Search user
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'fullName')))

        time.sleep(4)

        element.send_keys(username)

        time.sleep(2)

        #Open settings
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

        element.click()

        time.sleep(2)

        #GO to profile
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > .ng-scope:nth-child(2)')))

        element.click()

        time.sleep(2)

        #Go to documents
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Documents')))

        element.click()

        time.sleep(2)

        #Check elements present
        browser.find_element_by_css_selector(".title")
        browser.find_element_by_css_selector(".bundle-documents-button-text")
        browser.find_element_by_css_selector(".title")
        browser.find_element_by_css_selector(".grid-icon-refresh")
        browser.find_element_by_css_selector(".print-documents-button")
        browser.find_element_by_css_selector(".download-documents-button")
        browser.find_element_by_css_selector(".search-input")

if __name__ == '__main__':
    unittest.main()
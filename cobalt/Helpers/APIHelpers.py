"""Helper class for API requests"""

import requests
import json

class APIHelpers:

    def __init__(self):
        self.env = "tst"
        self.admin_username = "cobaltdltadmin02"
        self.admin_password = "Test123@"
        self.host = "idaas-api.cobalt-" + self.env + ".gesappsie.com"
        self.base_url = "https://api-a.cobalt-" + self.env + ".gesappsie.com"
    
    def createBeneficiaryWithCase(self, username):
        auth = self.login()
        employeeExists = self.checkIfEmployeeExists(auth, username)
        if len(employeeExists.json()) == 0:
            createBeneficiary = self.createBeneficiary(auth, username)
            if createBeneficiary != 200:
                return False

        createCase = self.createCase(auth, username)
        if createCase != 200:
            return False
        else:
            return True

    def checkIfEmployeeExists(self, auth, username):
        authorisedHeaders = {
            "accept": "application/json",
            "content-type": "application/json",
            "Authorization": auth
        }
        checkIfEmployeeExists = requests.get(self.base_url + "/api/ClientContacts/Query?companyId=3032&firstName=" + username + "&lastName=" + username + "&email=&employeeId=&query=&exactMatch=true", headers=authorisedHeaders)
        return checkIfEmployeeExists

    def login(self):

        requestHeaders ={
                "host": self.host,
                "accept": "*/*",
                "accept-language": "en-US,en;q=0.5",
                "accept-encoding": "gzip, deflate, br",
                "content-type": "application/x-www-form-urlencoded",
                # "origin": "https://cobalt-stg.gesappsie.com",
                "connection": "keep-alive"
            }
        loginResponse = requests.post("https://" + self.host + "/connect/token", data = {
            "client_id": "Automation",
            "username": self.admin_username,
            "password": self.admin_password,
            "grant_type": "password"
            }, headers=requestHeaders)

        auth = "Bearer " + loginResponse.json().get('access_token')
        return auth
    
    def createBeneficiary(self, auth, username):

        authorisedHeaders = {
            "accept": "application/json",
            "Authorization": auth,
            "content-type": "application/json"
        }

        payload = json.dumps({
            "active": True,
            "companyId": 3032,
            "email": username,
            "emails": [
                {
                    "emailAddress": username + "@deloitte.ie",
                    "isPrimary": True
                }
            ],
            "employeeNumber": None,
            "firstName": username,
            "isSSO": False,
            "isWebAccess": False,
            "jobTitle": None,
            "lastName": username,
            "middleName": None,
            "preferredName": None,
            "prefix": None,
            "primaryEmail": username + "@deloitte.ie",
            "roleDetails": [],
            "roleScope": None,
            "userType": "BENEFICIARY",
            "userType2": None})

        createBeneficiaryResponse = requests.post(self.base_url + "/api/ClientContacts", data=payload, headers=authorisedHeaders)

        return createBeneficiaryResponse.status_code

    def createCase(self, auth, username):
        
        authorisedHeaders = {
            "accept": "application/json",
            "content-type": "application/json",
            "Authorization": auth
        }

        getEmployeeDetailsResponse = requests.get(self.base_url + "/api/ClientContacts/Query?exactMatch=false&companyId=3032&query=" + username + " " + username + "&firstName=&lastName=&email=&employeeId=", headers=authorisedHeaders)
        companyContactId = getEmployeeDetailsResponse.json()[0].get('companyContactId')

        payload = json.dumps({
            "companyId":3032,
            "companyContactId":companyContactId,
            "newBeneficiary":
                {
                    "firstName":"ATYFSTUDBH",
                    "middleName":None,
                    "lastName":"ATYFSTUDBH",
                    "nationalities":[],
                    "employeeId":None,
                    "email":"ATYFSTUDBH@deloitte.ie",
                    "isSSO":False
                },
            "webAccess":False,
            "originCountryCode":None,
            "caseCountryCode":"GO",
            "caseTypeId":None,
            "jobPosition":None,
            "jobCatalogId":None,
            "salary":None,
            "salaryCurrency":"USD",
            "frequencyOfSalaryCode":"YEAR",
            "assignmentStartDate":None,
            "assignmentEndDate":None,
            "caseContacts":
            [
                {
                    "contactId":2,
                    "caseContactType":"BAL_MANAGER",
                    "isPrimary":True
                },
                {
                    "contactId":4,
                    "caseContactType":"BAL_ASSISTANT",
                    "isPrimary":True
                }
            ],
            "metaDataIds":[],
            "categoryId":None,
            "specialInstructions":None,
            "dependents":[],
            "questionnaires":[],
            "customFields":[],
            "workLocations":[],
            "immigrationProviderId":2,
            "isRenewal":False,
            "isCancellation":False,
            "immigrationDocumentIds":[],
            "managerContactId":None,
            "parentCaseId":None
        })

        createCaseResponse = requests.post(self.base_url + "/api/ClientCases", data=payload, headers=authorisedHeaders)

        return createCaseResponse.status_code

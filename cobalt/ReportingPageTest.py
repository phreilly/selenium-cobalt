import Helpers.Login as Login
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
from Helpers.TestManager import TestManager

#Test Case ID: 175250
class ReportingPageTest(unittest.TestCase):

    """These tests not created due to reporting bug - needs to be completed at later stage"""

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        self.driver  = webdriver.Firefox(options=fireFoxOptions)
        base_url = TestManager().get_base_url()
        self.url = base_url + "/#/dashboard/reporting"
        self.log_in = Login.AppLogin(self.driver)

    def test_run_all_projects(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(2)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'All_Projects')))

        element.click()


        return 
        

    def test_client_satisfaction_survey(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(2)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Client_Satisfaction_Survey')))

        element.click()
        return
    
    def test_gv_overview(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(2)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'GV_Overview')))

        element.click()
        return
    
    def test_ebills(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(2)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Ebill_Submitted_and_Approved')))

        element.click()
        return
    
    def test_immigration_docs(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(2)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Immigration_Document_Admin_Report')))

        element.click()
        return

    def test_projects_by_milestone(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(2)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Projects_by_Milestone')))

        element.click()
        return
    
    def test_attorney_view_filter_reports(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(2)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'All_Projects')))

        element.click()
        return

    def test_hrclient_view_filter_reports(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(2)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'All_Projects')))

        element.click()
        return


if __name__ == "__main__":
    unittest.main()
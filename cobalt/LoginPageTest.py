import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
from Helpers.TestManager import TestManager

#Test Case ID: 175249
class LoginTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        self.driver  = webdriver.Firefox(options=fireFoxOptions)
        self.url = TestManager().get_login_page_url()
        self.admin_email = "CobaltABTestAdmin@ga-testing.com"
        self.admin_pwd = "bKYsU33A@456"

    def test_page_elements_viewable(self):
        browser = self.driver
        browser.get(self.url)
        time.sleep(6)
            
        #test forgot username link viewable
        element = WebDriverWait(browser, 20).until(
            EC.element_to_be_clickable((By.ID, 'forgot-username-link')))
        
        time.sleep(5)
        element.click()

        time.sleep(3)
        assert 'member-support/1/456' in browser.current_url

        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-star-inserted:nth-child(1) > span")))

        element.click()

        time.sleep(2)

        #test forgot password link
        
        element = WebDriverWait(browser, 20).until(
            EC.element_to_be_clickable((By.LINK_TEXT, 'Forgot password')))
        
        element.click()

        time.sleep(1)
        assert 'forgotPassword' in browser.current_url

        time.sleep(4)

        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-star-inserted:nth-child(1) > span")))

        element.click()

        time.sleep(2)

        #Test member support link
        'Member support'
        element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.LINK_TEXT, 'Member support')))
        
        element.click()

        time.sleep(1)
        assert 'member-support' in browser.current_url

        time.sleep(4)

        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-star-inserted:nth-child(1) > span")))

        element.click()

    def test_login_to_cobalt(self):
        browser = self.driver
        browser.get(self.url)
        time.sleep(5)

        #Send email
        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.ID, "mat-input-0")))

        element.send_keys(self.admin_email)
        time.sleep(4)

        #Send password
        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.ID, "mat-input-1")))

        element.send_keys(self.admin_pwd)
        time.sleep(3)

        #Click Login
        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, ".green")))
        
        element.click()
        time.sleep(5)

        if browser.find_elements(By.XPATH, "//button[contains(.,'Skip for Now')]"):
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Skip for Now')]")))
            element.click()
            time.sleep(5)
        
        if browser.find_elements(By.XPATH, "//button[contains(.,'Confirm')]"):
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Confirm')]")))
            element.click()
            time.sleep(5)

        assert "IASTCPortal/home" in browser.current_url

        if browser.find_elements(By.XPATH, "//button[contains(.,'Skip for Now')]"):
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Skip for Now')]")))
            element.click()
            time.sleep(5)
        
        if browser.find_elements(By.XPATH, "//button[contains(.,'Confirm')]"):
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Confirm')]")))
            element.click()
            time.sleep(5)

        #Click on Immigration Tile
        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, ".links-no-child > .mat-list-item-content")))

        element.click()
        time.sleep(3)

        assert "home" in browser.current_url
        time.sleep(2)

    def test_login_internal(self):
        browser = self.driver
        browser.get(self.url)
        time.sleep(5)

        #Send email
        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.ID, "mat-input-0")))

        element.send_keys(self.admin_email)
        time.sleep(4)

        #Send password
        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.ID, "mat-input-1")))

        element.send_keys(self.admin_pwd)
        time.sleep(3)

        #Click Login
        element = WebDriverWait(browser, 8).until(
        EC.element_to_be_clickable((By.CSS_SELECTOR, ".green")))
        
        element.click()

        time.sleep(5)

        if browser.find_elements(By.XPATH, "//button[contains(.,'Skip for Now')]"):
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Skip for Now')]")))
            element.click()
            time.sleep(5)
        
        if browser.find_elements(By.XPATH, "//button[contains(.,'Confirm')]"):
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Confirm')]")))
            element.click()
            time.sleep(5)
        
        assert "IASTCPortal/home" in browser.current_url

        time.sleep(2)

    """  def test_login_external(self):
        browser = self.driver
        browser.get(self.url)
        time.sleep(5) """

if __name__ == "__main__":
    unittest.main()